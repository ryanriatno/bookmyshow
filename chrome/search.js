function highlightSearch(response) {
    var text = response;
    var query = new RegExp("(\\b" + text + "\\b)", "gim");
    var e = document.body.innerHTML;
    var enew = e.replace(/(<span>|<\/span>)/igm, "");
    document.body.innerHTML = enew;
    var newe = enew.replace(query, "<span class='highlight'>$1</span>");
    document.body.innerHTML = newe;

}
chrome.extension.onMessage.addListener(function(request, sender, sendResponse) {
  if (request.method == "search")
    highlightSearch(request.searchText);
  else
    sendResponse({}); // snub them.
});

